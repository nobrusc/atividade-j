<?php 


class DAO {

    protected $con = null;
    protected $host = 'localhost';
    protected $dbname = 'db_usuarios';
    protected $user = 'root';
    protected $password = 'root';
    

    public function openConnection() {

        try {

            $this->con = new PDO(
                'mysql:host='.$this->host.';dbname='.$this->dbname.';charset=utf-8',
                $this->user,
                $this->password
            );
   
   $this->con->exec("SET NAMES utf8");
   
            if(!$this->con) throw new Exception("Não foi possível contectar-se à base de dados.");
            return $this->con;

        } catch(PDOException $e ) {

            //tratar  p/ arquivo de log
            echo $e->getLine() ." ". $e->getMessage(); 
            exit();
            
        }

    }
    
    protected function closeConnection() {
        if($this->con != null) $this->con = null;
    }

    public function __destruct() {
        $this->closeConnection();
    }
}
?>